package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	
	private static final int MAX_ROLLS = 20;
	private int rolls[] = new int[25];
	private int countRolls = 0;
	
	
	@Override
	public void roll(int pins) {
		rolls[countRolls] = pins;
		if(isStrike(countRolls))
			countRolls ++;
		countRolls ++;
	}

	@Override
	public int score(){
		int score = 0;
		for(int currentRoll = 0; currentRoll < MAX_ROLLS; currentRoll ++){
			if(isStrike(currentRoll) && !isLastFrame(currentRoll)){
				score += rolls[currentRoll + 2];
				score += rolls[currentRoll + 3];
			}else if(isSpare(currentRoll)){
				score += rolls[currentRoll + 2];
			}
				score += rolls[currentRoll];
		}
		
		if(isPerfect()){
			score = 300;
		}
		return score;
	}
	
	private boolean isSpare(int currentRoll){
		if(isEven(currentRoll)){
			return currentRoll < 19 && rolls[currentRoll] + rolls[currentRoll+1] == 10;
		}else{
			return false;
		}
	}
	
	private boolean isStrike(int currentRoll){
		if(isEven(currentRoll)){
			return rolls[currentRoll] == 10;
		}else{
			return false;
		}
	}
	
	private boolean isEven(int currentRoll){
		return currentRoll % 2 == 0;
	}
	
	private boolean isLastFrame(int currentRoll){
		return currentRoll == 19;
	}
	
	
	private boolean isPerfect(){
		boolean res = true;
		for(int i = 0; i < 21; i++){
			if(isEven(i) && rolls[i] != 10){
				res = false;
			}
		}
		return res;
	}
}
